package lesson2.Permutation;

public class Permutation {
    public static boolean checkPermutation(int[] a, int[] b) {
        if (a == null || b == null || (a.length != b.length)){
            return false;
        }
        boolean matched;
        for (int i = 0; i < a.length; i++) {
            matched = false;
            for (int j = b.length - 1; j >= 0; j--) {
                if (a[i] == b[j]){
                    matched = true;
                    break;
                }
            }
            if (!matched) {
                return false;
            }
        }
        return true;
    }
}
