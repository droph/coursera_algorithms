package lesson2.StackWithMaxTask;

import java.util.Arrays;

public class StackWithMax {

    private double[] stack;
    private double max = Double.MIN_VALUE;
    private int maxCnt = 0;
    private int N = 0;

    public StackWithMax(){
        stack = new double[1];
    }

    public void push(double item){
        if (N == stack.length){
            resize(stack.length * 2);
        }
        if (item > max){
            max = item;
            maxCnt = 1;
        }else if(item == max){
            maxCnt++;
        }
        stack[N++] = item;
    }

    public double pop(){
        if(N > 0 && N == stack.length/4){
            resize(stack.length / 2);
        }
        double item = stack[--N];
        if (item == max){
            maxCnt--;
            if (maxCnt == 0){
                resetMax();
            }
        }
        return item;
    }

    public double max(){
        return max;
    }

    private void resetMax(){
        max = stack[0];
        for (int i = 1; i < N; i++){
            if (stack[i] > max){
                max = stack[i];
                maxCnt = 1;
            }else if(stack[i] == max){
                maxCnt++;
            }
        }
    }

    private void resize(int capacity){
        stack = Arrays.copyOf(stack, capacity);
    }

    public static void main(String[] args) {
        StackWithMax stackWithMax = new StackWithMax();
        stackWithMax.push(1);
        stackWithMax.push(2);
        stackWithMax.push(2);
        stackWithMax.push(1);
        System.out.println(stackWithMax.max());
    }
}
