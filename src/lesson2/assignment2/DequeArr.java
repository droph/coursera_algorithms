package lesson2.assignment2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeArr<Item> implements Iterable<Item> {

    private int left;
    private int right;
    private Item[] queue;
    private int mid;

    public DequeArr() {
        queue = (Item[]) new Object[2];
        mid = queue.length / 2;
    }

    public boolean isEmpty() {
        return (left + right) == 0;
    }

    public int size(){
        return (left + right);
    }

    public void addLast(Item item){
        if (left == mid){
            balance(1);
        }
        queue[mid - 1 - left++] = item;
    }

    public void addFirst(Item item){
        if (right == mid){
            balance(1);
        }
        queue[mid + right++] = item;
    }

    public Item removeLast(){
        if (isEmpty()){
            throw new NoSuchElementException();
        }
        Item item = queue[mid - left];
        queue[mid - left--] = null;
        if (left != 0 && mid / left >= 4){
            balance(0);
        }
        return item;
    }

    public Item removeFirst(){
        if (isEmpty()){
            throw new NoSuchElementException();
        }
        Item item = queue[mid + right - 1];
        queue[mid + right-- - 1] = null;
        if ( right != 0 && mid / right >= 4){
            balance(0);
        }
        return item;
    }

    @Override
    public Iterator<Item> iterator() {
        return null;
    }

    private void balance(int action){
        Item[] newQueue;
        int newLeft = 0;
        int newRight = 0;
        int newMid;
        if (action == 1) {
            if (left == 0 || right == 0 || (left / right >= 2 || right / left >= 2)) {
                newQueue = (Item[]) new Object[queue.length * 2];
            } else {
                newQueue = (Item[]) new Object[queue.length];
            }
        }else {
            newQueue = (Item[]) new Object[queue.length / 2];
        }
        newMid = newQueue.length / 2;
        int cnt = 0;
        int size = size();

        for(int i = mid - left; i < mid + right; i++) {
            if (cnt <= size() / 2) {
                newQueue[newMid - (size == 0 ? 1 : size) / 2 + newLeft++] = queue[i];
            }else {
                newQueue[newMid + newRight++] = queue[i];
            }
            cnt++;
        }
        queue = newQueue;
        mid = newMid;
        left = newLeft;
        right = newRight;
    }

    public static void main(String[] args) {
        DequeArr<String> deque = new DequeArr<>();
        deque.addFirst("A");
        deque.addFirst("B");
        deque.addFirst("C");
        deque.addFirst("D");
        deque.addFirst("F");
        deque.addFirst("G");
        System.out.println(deque.removeLast());
        System.out.println(deque.removeLast());
        System.out.println(deque.removeLast());
    }
}
