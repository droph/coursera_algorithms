package lesson2.assignment2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private Node<Item> first;
    private Node<Item> last;
    private int size;

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
       return size;
    }

    public void addLast(Item item) {
        nullCheck(item);
        Node<Item> newItem = new Node<>(item);
        if (last == null) {
            last = newItem;
            first = newItem;
        } else {
            Node<Item> newNext = last;
            last.prev = newItem;
            last = last.prev;
            last.next = newNext;
        }
        size++;
    }

    public void addFirst(Item item) {
        nullCheck(item);
        Node<Item> newItem = new Node<>(item);
        if (first == null) {
            last = newItem;
            first = newItem;
        } else {
            Node<Item> newPrev = first;
            first.next = newItem;
            first = first.next;
            first.prev = newPrev;
        }
        size++;
    }

    public Item removeLast() {
        noElemCheck();
        Node<Item> item = last;
        if (size == 1) {
            last = null;
            first = null;
        } else {

            last = last.next;
            last.prev = null;
        }
        size--;
        return item.value;
    }

    public Item removeFirst() {
        noElemCheck();
        Node<Item> item = first;
        if (size == 1) {
            last = null;
            first = null;
        } else {
            first = first.prev;
            first.next = null;
        }
        size--;
        return item.value;
    }

    @Override
    public Iterator<Item> iterator() {
        return new Iterator<Item>() {
            private Node<Item> current = first;
            @Override
            public boolean hasNext() {
                return current != null;
            }
            @Override
            public Item next() {
                if (current == null) {
                    throw new NoSuchElementException();
                }
                Item item = current.value;
                current = current.prev;
                return item;
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    private void nullCheck(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
    }

    private void noElemCheck() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
    }

    private class Node<Item> {
        private Item value;
        private Node<Item> next;
        private Node<Item> prev;
        public Node(Item value) {
            this.value = value;
        }
    }

    public static void main(String[] args) {
        Deque<String> q = new Deque<>();
        q.addLast("A");
        q.addLast("B");
        q.addLast("C");
        q.addFirst("V");
        q.addFirst("X");
        q.addFirst("Z");
        for (String s : q) {
            System.out.println(s);
        }
    }
}
