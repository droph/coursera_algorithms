package lesson2.assignment2;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] queue;
    private int m;

    public RandomizedQueue() {
        queue = (Item[]) new Object[1];
    }
    public boolean isEmpty() {
        return m == 0;
    }
    public int size() {
        return m;
    }
    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (m == queue.length) {
            resize(queue.length * 2);
        }
        queue[m++] = item;
    }

    public Item dequeue() {
        chkSize();
        int n = StdRandom.uniform(m);
        Item item = queue[n];
        queue[n] = queue[m - 1];
        queue[--m] = null;
        if (queue.length / 4 >= m) {
            resize(queue.length / 2);
        }
        return item;
    }
    public Item sample() {
        chkSize();
        return queue[StdRandom.uniform(m)];
    }

    private void resize(int size) {
        queue = Arrays.copyOf(queue, size);
    }

    private void chkSize() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
    }

    @Override
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private class RandomIterator implements Iterator<Item> {

        private int cnt;
        private Item[] rndQueue;

        public RandomIterator() {
            cnt = m;
            rndQueue = Arrays.copyOf(queue, m);
            for (int i = m - 1; i > 0; i--) {
                int index = StdRandom.uniform(i + 1);
                Item rnd = rndQueue[index];
                rndQueue[index] = rndQueue[i];
                rndQueue[i] = rnd;
            }
        }

        @Override
        public boolean hasNext() {
            return cnt > 0;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return rndQueue[--cnt];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(String[] args) {
        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<>();
        randomizedQueue.enqueue("A");
        randomizedQueue.enqueue("B");
        randomizedQueue.enqueue("C");
        randomizedQueue.enqueue("D");
        System.out.println("Sample");
        for (int i = 0; i < 2; i++) {
            System.out.println(randomizedQueue.sample());
        }
        System.out.println("Deque");
        for (int i = 0; i < 2; i++) {
            System.out.println(randomizedQueue.dequeue());
        }
        randomizedQueue.enqueue("R");
        randomizedQueue.enqueue("T");
        randomizedQueue.enqueue("Y");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                StringBuilder sb = new StringBuilder();
                for (String s : randomizedQueue) {
                    sb.append(s + " ");
                }
                System.out.println(Thread.currentThread().getName() + " - " + sb.toString());
            }
        };
        System.out.println("Iter");
        for (int i = 0; i < 3; i++) {
            new Thread(runnable).start();
        }

    }
}
