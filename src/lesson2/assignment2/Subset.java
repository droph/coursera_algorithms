package lesson2.assignment2;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Subset {
    public static void main(String[] args) {
        if (args.length == 1) {
            int k = Integer.parseInt(args[0]);
            RandomizedQueue<String> queue = new RandomizedQueue<>();
            while (!StdIn.isEmpty()) {
                queue.enqueue(StdIn.readString());
            }
            int iter = k > queue.size() ? queue.size() : k;
            int i = 0;
            for (String s : queue) {
                if (iter > i++) {
                    StdOut.println(s);
                }else {
                    break;
                }
            }
        }
    }
}
