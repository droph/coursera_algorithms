package lesson2.twoStacksQueue;

import java.util.Stack;

public class Queue<T> {

    private Stack<T> queueStack = new Stack<T>();
    private Stack<T> dequeueStack = new Stack<T>();


    public boolean isEmpty() {
        return dequeueStack.isEmpty() && queueStack.isEmpty();
    }

    public void enqueue(T obj) {
        queueStack.push(obj);
    }

    public T dequeue() {
        if (dequeueStack.isEmpty()){
            while (!queueStack.isEmpty()){
                dequeueStack.push(queueStack.pop());
            }
        }
        return dequeueStack.pop();
    }
}
