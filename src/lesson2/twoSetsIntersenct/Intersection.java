package lesson2.twoSetsIntersenct;

public class Intersection {

    public static int checkIntersect(Point2D[] a, Point2D[] b){
        int cnt = 0;
        for (int i = 0; i < a.length; i++){
            for (int j = 0; j < b.length; j++){
                if (a[i].x == b[j].x && a[i].y == b[j].y){
                    cnt++;
                    break;
                }
            }
        }
        return cnt;
    }

    public class Point2D{
        private final double x;
        private final double y;

        public Point2D(double x, double y){
            this.x = x;
            this.y = y;
        }
    }
}
