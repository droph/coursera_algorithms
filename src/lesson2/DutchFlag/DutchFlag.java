package lesson2.DutchFlag;

public class DutchFlag {
    private int[] a;

    public DutchFlag(int[] a){
        this.a = a;
    }
    public void sort(){
        int curPos = 0;
        int minPos = 0;
        int maxPos = a.length - 1;
        int mid = 1;
        while (curPos <= maxPos) {
            if (a[curPos] < mid) {
                swap(curPos++, minPos++);
            }else if(a[curPos] > mid){
                swap(curPos, maxPos--);
            }else {
                curPos++;
            }
        }
    }
    public void swap(int i, int j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public int color(int i){
        return a[i];
    }
}
