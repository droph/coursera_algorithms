package lesson1.assignment1;

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private double[] thresholds;
    private int n;
    private int trials;

    public PercolationStats(int n, int trials) {    // perform trials independent experiments on an n-by-n grid
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }
        this.trials = trials;
        this.n = n;
        thresholds = new double[trials];
        trial();
    }
    public double mean() {                           // sample mean of assignment1 thresholds
        return StdStats.mean(thresholds);
    }

    public double stddev() {                         // sample standard deviation of assignment1 thresholds
        return StdStats.stddev(thresholds);
    }
    public double confidenceLo() {                   // low  endpoint of 95% confidence interval
        return mean() -  getConfDev();
    }
    public double confidenceHi() {                   // high endpoint of 95% confidence interval
        return mean() +  getConfDev();
    }
    private double getConfDev() {
        return (stddev() * 1.96) / Math.sqrt(n);
    }
    private void trial() {
        Percolation percolation;
        int total = n * n;
        int x;
        int y;

        for (int i = 0; i < trials; i++) {
            StdRandom.setSeed(i);
            percolation = new Percolation(n);
            int counter = 0;

            while (!percolation.percolates()) {
                do {
                    x = getRandom();
                    y = getRandom();
                } while (percolation.isOpen(y, x));
                percolation.open(y, x);
                counter++;
            }
            if (percolation.percolates()) {
                thresholds[i] = (double) counter / total;
            }
        }
    }

    private int getRandom() {
        int r;
        while ((r = StdRandom.uniform(n + 1)) == 0) {
            // empty loop to ensure that 0 won't be returned
        }
        return r;
    }

    public static void main(String[] args) {        // test client (described below)

        if (args.length == 2) {

            int n = Integer.parseInt(args[0]);
            int trials = Integer.parseInt(args[1]);
            PercolationStats percolationStats = new PercolationStats(n, trials);
            System.out.println(String.format("%% java PercolationStats   %d %d", n, trials));
            System.out.println(String.format("mean                    = %f", percolationStats.mean()));
            System.out.println(String.format("stddev                  = %f", percolationStats.stddev()));
            System.out.println(String.format("95%% confidence interval = %f, %f",
                    percolationStats.confidenceLo(), percolationStats.confidenceHi()));
        }
    }
}
