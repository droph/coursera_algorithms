package lesson1.assignment1;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private int n;
    private int total;
    private int[] stats;
    private WeightedQuickUnionUF unionUF;


    public Percolation(int n) {              // create n-by-n grid, with all sites blocked
        if (n <= 0) {
            throw new IllegalArgumentException();
        }
        this.n = n;
        total = n * n;
        unionUF = new WeightedQuickUnionUF(total + 2); // 0 for top, +1 for bottom
        stats = new int[total + 2];
        stats[0] = 1;
        stats[total + 1] = 1;
    }

    public void open(int i, int j) {         // open site (row i, column j) if it is not open already
        // Check to avoid virtual elements appearing
        if (i == 0 || i == n + 1) {
            throw new IndexOutOfBoundsException();
        }
        if (!isOpen(i, j)) {
            int idx = xyTo1D(i, j);
            stats[idx] = 1;
            unionIfOpen(i - 1, j, idx); // Top
            unionIfOpen(i + 1, j, idx); // Bottom
            unionIfOpen(i, j - 1, idx); // Left
            unionIfOpen(i, j + 1, idx); // Right
        }
    }

    public boolean isOpen(int i, int j) {     // is site (row i, column j) open?
        validateIndices(i, j);
        int idx = xyTo1D(i, j);
        return stats[idx] == 1;
    }

    public boolean isFull(int i, int j) {     // is site (row i, column j) full?
        validateIndices(i, j);
        int idx = xyTo1D(i, j);
        return unionUF.connected(0, idx);
    }

    public boolean percolates() {             // does the system percolate?
        return unionUF.connected(0, total + 1);
    }

    private void validateIndices(int i, int j) {
        if (i <= 0 || i > n || j <= 0 || j > n) {
            throw new IndexOutOfBoundsException();
        }
    }

    private boolean isOpenInt(int i, int j) {
        if (i < 0 || i > n + 1 || j <= 0 || j > n) {
            throw new IndexOutOfBoundsException();
        }
        int idx = xyTo1D(i, j);
        return stats[idx] == 1;
    }

    private int xyTo1D(int y, int x) {
        int idx;

        if (y == 0) {
            idx = y;
        } else if (y == n + 1) {
            idx = total + 1;
        } else {
            idx = (y - 1) * n + x;
        }
        return idx;
    }

    private void unionIfOpen(int y, int x, int idx) {
        if (x > 0 && x <= n) {
            if (isOpenInt(y, x) || y == 0 || y == n + 1) {
                unionUF.union(xyTo1D(y, x), idx);
            }
        }
    }

    public static void main(String[] args) {  // test client (optional)
        Percolation percolation = new Percolation(5);
        percolation.open(1, 1);
        percolation.open(2, 1);
        percolation.open(3, 1);
        percolation.open(4, 1);
        percolation.open(5, 1);
        percolation.open(5, 4);
        System.out.println(percolation.percolates());
        System.out.println(percolation.isOpen(1, 2));
        System.out.println(percolation.unionUF.find(26));
    }
}

