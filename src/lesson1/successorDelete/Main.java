package lesson1.successorDelete;

public class Main {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        SuccessorDelete successorDelete = new SuccessorDelete(arr);
        System.out.println(successorDelete.findSuccessor(4));
        System.out.println(successorDelete.findSuccessor(5));
        successorDelete.delete(5);
        System.out.println(successorDelete.findSuccessor(4));
        System.out.println(successorDelete.findSuccessor(5));
        successorDelete.delete(4);
        System.out.println(successorDelete.findSuccessor(9));
    }
}
