package lesson1.successorDelete;

public class SuccessorDelete {

    private int[] ids;
    private int[] successors;

    public SuccessorDelete(int[] S){
        ids = new int[S.length];
        successors = new int[ids.length];

        for (int i = ids.length - 1; i > 0; i--){
            ids[i] = 1;
            successors[i - 1] = i;
        }
        ids[0] = 1;
        successors[ids.length - 1] = -1; // Biggest value has no successor
    }

    public int findSuccessor(int x){
        if (ids[x] == 0 || x > ids.length - 1){
            return -1; //Record is not available
        }
        return successors[x];
    }

    public void delete(int x){
        if (ids[x] != 0){
            ids[x] = 0;
            if (x > 0) {
                if (ids[x - 1] == 0) {
                    successors[successors[x - 1]] = successors[x];
                    successors[x] = successors[x - 1];
                } else {
                    successors[x - 1] = successors[x];
                    successors[x] = x - 1;
                }
            }
        }
    }

    public void draw(){
        System.out.println("Numbers..");
        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", i));
        }
        System.out.println();

        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", ids[i]));
        }
        System.out.println();
        System.out.println("Successors..");
        for (int i = 0; i < successors.length; i++){
            System.out.print(String.format("%3d", successors[i]));
        }
        System.out.println();
    }
}
