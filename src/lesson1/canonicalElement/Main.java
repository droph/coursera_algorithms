package lesson1.canonicalElement;

public class Main {
    public static void main(String[] args) {

        CanonicalElement socialNetwork = new CanonicalElement(10);
        System.out.println("================================");

        socialNetwork.getStats();
        socialNetwork.union(8,2);
        socialNetwork.union(9,2);
        socialNetwork.union(5,6);
        socialNetwork.union(8,5);
        socialNetwork.union(0,7);
        socialNetwork.union(0,3);
        socialNetwork.getStats();

    }
}
