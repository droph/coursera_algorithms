package lesson1.canonicalElement;

public class CanonicalElement {
    private int[] ids;
    private int[] szs;
    private int[] lgs;

    public CanonicalElement(int N){
        ids = new int[N];
        szs = new int[N];
        lgs = new int[N];

        for (int i = 0; i < N; i++){
            ids[i] = i;
            lgs[i] = i;
            szs[i] = 1;
        }
    }

    public int root(int i){
        while(i != ids[i]){
            ids[i] = ids[ids[i]];
            i = ids[i];
        }
        return i;
    }

    public boolean connected(int p, int q){
        return root(p) == root(q);
    }

    public void union(int p, int q){
        int i = root(p);
        int j = root(q);

        if (i == j){
            return;
        }
        if(szs[i] < szs[j]){
            ids[i] = j;
            szs[j] += szs[i];
            lgs[j] = max(i, j);
        }else {
            ids[j] = i;
            szs[i] += szs[j];
            lgs[i] = max(i, j);
        }
    }
    private int max(int i, int j){
        return lgs[j] > lgs[i] ? lgs[j] : lgs[i];
    }

    public int find(int i){
        return lgs[root(i)];
    }

    public void draw(){
        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", i));
        }
        System.out.println();

        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", ids[i]));
        }
        System.out.println();
    }

    public void getStats(){
        draw();
        for (int i = 0; i < ids.length; i++){
            if (ids[i] == i){
                System.out.println("Root " + i);
                System.out.println("Size " + szs[i]);
                System.out.println("Biggets " + find(i));
            }
        }
    }
}
