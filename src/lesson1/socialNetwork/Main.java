package lesson1.socialNetwork;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {

        SocialNetwork socialNetwork = new SocialNetwork(10);
        System.out.println("================================");

        socialNetwork.union(8,2, LocalDateTime.now());
        socialNetwork.union(9,2, LocalDateTime.now());
        socialNetwork.union(5,6, LocalDateTime.now());
        socialNetwork.union(8,5, LocalDateTime.now());
        socialNetwork.union(0,7,LocalDateTime.now());
        socialNetwork.union(0,3,LocalDateTime.now());
        socialNetwork.getStats();
        System.out.println(socialNetwork.root(5));
        System.out.println(socialNetwork.getEarliestFriendshipTime(5));
        System.out.println(socialNetwork.root(6));
        System.out.println(socialNetwork.getEarliestFriendshipTime(8));


    }
}
