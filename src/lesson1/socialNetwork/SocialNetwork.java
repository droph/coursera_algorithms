package lesson1.socialNetwork;

import java.time.LocalDateTime;

public class SocialNetwork
{
    private int[] ids;
    private int[] szs;
    private LocalDateTime[] meetTimes;

    public SocialNetwork(int N){
        ids = new int[N];
        szs = new int[N];
        meetTimes = new LocalDateTime[N];

        for (int i = 0; i < N; i++){
            ids[i] = i;
            szs[i] = 1;
        }
    }

    public int root(int i){
        while(i != ids[i]){
            ids[i] = ids[ids[i]];
            i = ids[i];
        }
        return i;
    }

    public boolean connected(int p, int q){
        return root(p) == root(q);
    }

    public void union(int p, int q, LocalDateTime meetTime){
        int i = root(p);
        int j = root(q);

        if (i == j){
            return;
        }
        if(szs[i] < szs[j]){
            ids[i] = j;
            szs[j] += szs[i];
            meetTimes[j] = meetTime;
        }else {
            ids[j] = i;
            szs[i] += szs[j];
            meetTimes[i] = meetTime;
        }
    }

    public LocalDateTime getEarliestFriendshipTime(int id){
        return meetTimes[root(id)];
    }

    public void draw(){
        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", i));
        }
        System.out.println();

        for (int i = 0; i < ids.length; i++){
            System.out.print(String.format("%3d", ids[i]));
        }
        System.out.println();
    }

    public void getStats(){
        draw();
        for (int i = 0; i < ids.length; i++){
            if (ids[i] == i){
                System.out.println("Root " + i);
                System.out.println("Size " + szs[i]);
                System.out.println("MeetTime " + meetTimes[i]);
            }
        }
    }
}
