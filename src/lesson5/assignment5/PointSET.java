package lesson5.assignment5;

import edu.princeton.cs.algs4.*;

public class PointSET{
    private SET<Point2D> set;

    public PointSET(){ set = new SET<>(); }

    public boolean isEmpty(){ return set.isEmpty(); }

    public int size() { return set.size(); }

    public void insert(Point2D p){ set.add(p); }

    public boolean contains(Point2D p){ return set.contains(p); }

    public void draw(){
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        for (Point2D p : set) {
            StdDraw.point(p.x(), p.y());
        }
        StdDraw.show(0);
    }

    public Iterable<Point2D> range(RectHV rect) {
        Stack<Point2D> s = new Stack<>();
        for (Point2D p : set) {
            if (rect.contains(p)){
                s.push(p);
            }
        }
        return s;
    }

    public Point2D nearest(Point2D p) {
        if (isEmpty()) {
            return null;
        }

        double dis;
        double minDis = Double.MAX_VALUE;

        Point2D q = null;

        for (Point2D ip : set) {
            dis = p.distanceSquaredTo(ip);
            if (dis < minDis) {
                minDis = dis;
                q = ip;
            }
        }
        return q;
    }
}
